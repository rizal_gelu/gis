package models

import (
    // "fmt"
    "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "time"
)

type Timeline struct {
	Id_timeline 	string `orm:"pk"`
	Id_profile 		string `orm:"null"`
	Description  	string `orm:"null"`
	File 			string `orm:"null"`
	Date_created	time.Time `orm:"null"`
}

func init () {
	orm.RegisterModel(new(Timeline))
}