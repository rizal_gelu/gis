package models

import (
	"github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    // "time"
    // "github.com/astaxie/beego"
	// "gis/helpers"
)

type Security_keys struct{
	Id 			                int 
    Token       	           	string 
	Id_user						int
	Date_created				int64
}

func (m *Security_keys) Insert() error {
    if _, err := orm.NewOrm().Insert(m); err != nil {
        return err
    }
    return nil
}

func (m *Security_keys) Read(fields ...string) error {
    if err:= orm.NewOrm().Read(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Security_keys) ReadOrCreate(field string, fields ...string) (bool, int64, error) {
    return orm.NewOrm().ReadOrCreate(m, field, fields...)
}

func (m *Security_keys) Update(fields ...string) error {
    if _, err := orm.NewOrm().Update(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Security_keys) Delete() error {
    if _, err := orm.NewOrm().Delete(m); err != nil {
        return err
    }
    return nil
}

func JwtCheck() orm.QuerySeter {
    var table Security_keys
    return orm.NewOrm().QueryTable(table).OrderBy("-Id")
}

func init () {
	orm.RegisterModel(new(Security_keys))
    // orm.RegisterModelWithPrefix(
    //     beego.AppConfig.String("dbprefix"),
    //     new(Security_keys))
}