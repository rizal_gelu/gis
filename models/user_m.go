package models

import (
    "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    // "time"
    "github.com/astaxie/beego"
)

type Users struct{
	Id 			                int 
    Ip_address                  string 
	Email  		                string `orm:"size(64);unique" form:"Email" valid:"Required;Email"`
    Username                    string 
    Activation_code             string 
    Forgotten_password_code     string
    Forgotten_password_time     int64
    Password   	                string `form:"password"`
    First_name 	                string `form:first_name`
    Last_name 	                string `form:last_name`
    Company 	                string `form:company`
    Last_login                  int64
    Active                      int
    Created_on                  int64
}

func (m *Users) Insert() error {
    if _, err := orm.NewOrm().Insert(m); err != nil {
        return err
    }
    return nil
}

func (m *Users) Read(fields ...string) error {
    if err:= orm.NewOrm().Read(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Users) ReadOrCreate(field string, fields ...string) (bool, int64, error) {
    return orm.NewOrm().ReadOrCreate(m, field, fields...)
}

func (m *Users) Update(fields ...string) error {
    if _, err := orm.NewOrm().Update(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Users) Delete() error {
    if _, err := orm.NewOrm().Delete(m); err != nil {
        return err
    }
    return nil
}

func User_check() orm.QuerySeter {
    var table Users
    return orm.NewOrm().QueryTable(table).OrderBy("-Id")
}

func init () {
	// orm.RegisterModel(new(Users))
    orm.RegisterModelWithPrefix(
        beego.AppConfig.String("dbprefix"),
        new(Users))
}