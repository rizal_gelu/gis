package models

import (
	// "fmt"
    "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "time"
)

type Security_question struct {
	Id_security_question 	string `orm:"pk"`
	Question 				string
	Create_by				string
	Update_by				string
	Date_created			time.Time
	Date_updated    		time.Time
}

func init () {
	orm.RegisterModel(new(Security_question))
}
