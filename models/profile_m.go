package models

import (
	// "http"
	// "github.com/gorilla/rpc"
	// "github.com/gorilla/rpc/json"
    "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "github.com/astaxie/beego"
    "time"
)

type Profile struct{
	Id_profile 				string `orm:"pk"`
	First_name 				string
	Last_name				string
	Avatar					string
	Email					string `orm:"size(64);unique" form:"Email" valid:"Required;Email"`
	Email_secondary    		string
	Date_of_birth			time.Time `orm:"null"`
	Bio						string
	Id_province				string
	Id_district				string
	Id_sub_district 		string
	Id_village				string
	Status					int
	Date_created 			time.Time
}

func (m *Profile) Insert() error {
    if _, err := orm.NewOrm().Insert(m); err != nil {
        return err
    }
    return nil
}

func (m *Profile) Read(fields ...string) error {
    if err:= orm.NewOrm().Read(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Profile) ReadOrCreate(field string, fields ...string) (bool, int64, error) {
    return orm.NewOrm().ReadOrCreate(m, field, fields...)
}

func (m *Profile) Update(fields ...string) error {
    if _, err := orm.NewOrm().Update(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Profile) Delete() error {
    if _, err := orm.NewOrm().Delete(m); err != nil {
        return err
    }
    return nil
}

func Profiles() orm.QuerySeter {
    var table Profile
    return orm.NewOrm().QueryTable(table).OrderBy("-Id_profile")
}

func init() {
	// orm.RegisterModel(new(Profile))
    orm.RegisterModelWithPrefix(
        beego.AppConfig.String("dbprefix"),
        new(Profile))
}