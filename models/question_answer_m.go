package models

import (
    // "fmt"
    "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "time"
    "github.com/astaxie/beego"
)

type Question_answer struct {
	Id_question_answer 		string `orm:"pk"`
	Id_security_question 	string `orm:"null"`
	Id_user  				int    `orm:"null"`
	Answer 					string `orm:"null"`
	Date_created			time.Time `orm:"null"`
	Date_updated    		time.Time `orm:"null"`
}

func (m *Question_answer) Insert() error {
    if _, err := orm.NewOrm().Insert(m); err != nil {
        return err
    }
    return nil
}

func (m *Question_answer) Read(fields ...string) error {
    if err:= orm.NewOrm().Read(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Question_answer) ReadOrCreate(field string, fields ...string) (bool, int64, error) {
    return orm.NewOrm().ReadOrCreate(m, field, fields...)
}

func (m *Question_answer) Update(fields ...string) error {
    if _, err := orm.NewOrm().Update(m, fields...); err != nil {
        return err
    }
    return nil
}

func (m *Question_answer) Delete() error {
    if _, err := orm.NewOrm().Delete(m); err != nil {
        return err
    }
    return nil
}

func Question_answers() orm.QuerySeter {
    var table Question_answer
    return orm.NewOrm().QueryTable(table).OrderBy("-Id_question_answer")
}

func init () {
	// orm.RegisterModel(new(Question_answer))
    orm.RegisterModelWithPrefix(
        beego.AppConfig.String("dbprefix"),
        new(Question_answer))
}