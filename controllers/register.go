package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	// "github.com/astaxie/beego/context"
	"github.com/astaxie/beego/orm"
    // _ "github.com/go-sql-driver/mysql"
    "gis/models"
    "gis/helpers"
    // "gis/library"
    "time"
)

type RegisterController struct {
	beego.Controller
}

type RegisterProcessController struct {
	beego.Controller
}

func (c *RegisterController) Get() {
	// Get Security Question
	o := orm.NewOrm()
	var ListQuestion []*models.Security_question
	_, err := o.QueryTable("security_question").All(&ListQuestion)
	if err != nil {
		fmt.Println(err.Error())
	}

	flash := beego.ReadFromRequest(&c.Controller)
	if _, ok := flash.Data["error"]; ok {
		c.Data["Title"] 	= "Register - Gelhosting"
		c.Data["Question"]	= ListQuestion
		c.TplName			= "register/register.tpl"
    } else {
    	c.Data["Title"] 	= "Register - Gelhosting"
		c.Data["Question"]	= ListQuestion
		c.TplName			= "register/register.tpl"
    }
}

func (this *RegisterProcessController) Post() {
	o := orm.NewOrm()
	o.Using("default")

	// Uploaded file (avatar)
	flash := beego.NewFlash() // Initialization flash
	f, h, check_value := this.GetFile("avatar")  // Get file form input html
	if check_value != nil { // checking data image

		flash.Error("Need Photo Avatar!") // Initialization message error (for showing to interface user)
	    flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/register", 302) // redirect to function this function if err.

	} else {
		
		path := "./static/avatar/" + h.Filename   // making path file (Name directory + name.file)
		f.Close() // End command
		this.SaveToFile("avatar", path)    // Save file to directory.

		hash, _ 	:= helpers.HashPassword(this.GetString("password")) // Send password to func hashAndSalt
		first_name 	:= this.GetString("first_name")
		last_name 	:= this.GetString("last_name")
		email 		:= this.GetString("email")
		company 	:= this.GetString("company")

		s 					:= this.Ctx.Input.IP() // Get Ip

		register_user 							:= new(models.Users) // declaration User Struct in package models
		register_user.First_name 				= first_name
		register_user.Ip_address 				= s
		register_user.Last_name 				= last_name
		register_user.Email 					= email
		register_user.Username 					= first_name+last_name // to combine 2 variables we can use "+". it's the same as javascript integration
		register_user.Password 					= hash
		register_user.Company 					= company
		register_user.Activation_code 			= helpers.RandomString(32)
		register_user.Active 					= 0 // if 0 account has not actived (default)
		register_user.Created_on 				= time.Now().UnixNano() / int64(time.Millisecond) // type data int64

		var user models.Users
		checking_email := o.QueryTable("users").Filter("email", email).One(&user) // checking email register

		if checking_email == orm.ErrMultiRows {
		    fmt.Println("Returned Multi Rows Not One") 
		} else if checking_email == orm.ErrNoRows { // if no row found (the process continues)

			_, error := o.Insert(register_user) // insert to table user
			if error != nil {
				fmt.Println(error)
			}

			// Generate uuid Id_Security_question, Id_profile
			Id_question_answer 	 	:= helpers.Uuid() // Parsing proses to func uuid, and uuid return random string
			Id_profile 				:= helpers.Uuid() // Parsing proses to func uuid, and uuid return random string

			// Convert string to date
			date_of_birth 	:= this.GetString("date_of_birth") // Get value from html 
			t, fail 		:= time.Parse("01/02/2006", date_of_birth) // Format variable date_of_birth (string) to time type (yyyy-mm-dd)
			if fail != nil { // checking if not equal nil. Sometimes, for writing time.parse, we can write just single variable like :
				// t, _ 		:= time.Parse("01/02/2006", date_of_birth), and if we write like this, then we don't need to check for errors or fail
				// just use variable "t" for get value.
				fmt.Println(fail) // print fail
			} 

			register_profile 						:= new(models.Profile) // declaration Profile Struct in package models
			register_profile.Id_profile 			= Id_profile
			register_profile.First_name				= this.GetString("first_name")
			register_profile.Last_name				= this.GetString("last_name")
			register_profile.Avatar					= h.Filename // get file name for input to db
			register_profile.Email					= this.GetString("email")
			register_profile.Email_secondary		= this.GetString("email_secondary")
			register_profile.Date_of_birth 			= t 
			register_profile.Bio 					= this.GetString("bio")
			register_profile.Id_province 			= this.GetString("id_province")
			register_profile.Id_district 			= this.GetString("id_district")
			register_profile.Id_sub_district 		= this.GetString("id_sub_district")
			register_profile.Id_village 			= this.GetString("id_village")
			register_profile.Status 				= 0
			register_profile.Date_created 			= time.Now()

			o.Insert(register_profile) // insert to table profile

			var getId_user models.Users
			o.QueryTable("users").Filter("email", email).One(&getId_user) // get id_user

			// insert data to answer_question
			default_date_format, _ 		:= time.Parse("yyyy-mm-dd", "0000-00-00") // set time default "0000-00-000"
			answer 						:= new(models.Question_answer) // Declaration Question_answer struct
			answer.Id_question_answer 	= Id_question_answer // Using Id_security_question (Random String UUID)
			answer.Id_user  			= getId_user.Id // Using id_user
			answer.Answer 				= this.GetString("answer")
			answer.Id_security_question	= this.GetString("id_security_question")
			answer.Date_updated 		= default_date_format // use variable default_date_format (time parse), cause field date_update cant be null
			answer.Date_created			= time.Now() // format time now 

			_, err := o.Insert(answer) // insert to table answer_question	
			
			if err != nil {
				fmt.Println(err)
			}
		}
	}

	this.Ctx.Redirect(302, "/login") // redirect to login
}