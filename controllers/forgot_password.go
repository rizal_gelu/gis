package controllers

import (
	// "fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "gis/models"
    "gis/helpers"

    "strconv"
)

type ForgotPasswordController struct {
	beego.Controller
}

type ForgotPasswordProcessController struct {
	beego.Controller
}

type ResetPassword struct {
	beego.Controller
}

func (c *ForgotPasswordController) Get() {
	flash := beego.ReadFromRequest(&c.Controller)
	if _, ok := flash.Data["error"]; ok {
		c.Data["Title"] = "Forgot Password - Gelhosting"
		c.TplName 		= "forgot_password/forgot_password.tpl"
	} else {	
		c.Data["Title"] = "Forgot Password - Gelhosting"
		c.TplName 		= "forgot_password/forgot_password.tpl"
	}
}

func (this *ForgotPasswordProcessController) Post() {
	flash 	:= beego.NewFlash() // Initialization flash
	o		:= orm.NewOrm()
	o.Using("belajar_beego")

	email 		:= this.GetString("email")

	var user models.Users
	checking_email := o.QueryTable("users").Filter("email", email).One(&user) // checking email register
	if checking_email == orm.ErrNoRows {
		flash.Error("Your email is not registered !") // Initialization message error (for showing to interface user)
	    flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/forgot_password", 302) // redirect to function this function if err.
	} 

	if user.Active != 0 {

		ip := this.Ctx.Input.IP()
		forgot_password 							:= models.Users{Id : user.Id}
		// forgot_password.Id 							= user.Id
		forgot_password.Ip_address 					= ip
		forgot_password.Forgotten_password_code 	= helpers.RandomString(32)

		o.Update(&forgot_password, "ip_address", "forgotten_password_code")

		flash.Notice("Check Your Email")
		flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/login", 302) 
	} else {
		flash.Error("Your account has not been activated !")
		flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/forgot_password", 302) 
	}
}

func (this *ResetPassword) Get() {
	flash 	:= beego.NewFlash()
	id 		:= this.Ctx.Input.Param(":id") // the variable "id" was originally a string
	code 	:= this.Ctx.Input.Param(":code")

	id_user, _ := strconv.Atoi(id) // Convert variable id string -> int (the variable "id" was originally a string)

	o := orm.NewOrm()
	o.Using("belajar_beego")

	user := models.Users{Id: id_user, Forgotten_password_code: code} // This is STRUCT Users
	err := o.Read(&user) // ORM read
	if err == orm.ErrNoRows {
    	flash.Error("No result found !") // Initialization message error (for showing to interface user)
	    flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/login", 302) // redirect to function this function if err.
	} else if err == orm.ErrMissPK {
	    flash.Error("No primary key found !") // Initialization message error (for showing to interface user)
	    flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/login", 302) // redirect to function this function if err.
	} else {
		this.Data["Title"] 				= "Change Your Password - Gelhosting"
		this.TplName					= "forgot_password/change_password.tpl"
	}
}