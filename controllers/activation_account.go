package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "gis/models"
    // "gis/helpers"
    "time"
    "os"
    "strconv" // string converter
)

type ActivationAccount struct {
	beego.Controller
}

type Change_password struct {
	beego.Controller
}

func (this *ActivationAccount) Get() {
	flash := beego.NewFlash()
	id := this.Ctx.Input.Param(":id") // the variable "id" was originally a string
	code := this.Ctx.Input.Param(":code")

	// why do we have to convert the variable? Because the data type in "User Struct" is int
	// and command "this.Ctx.Input.Param" produce type data string.
	id_user, _ := strconv.Atoi(id) // Convert variable id string -> int (the variable "id" was originally a string)

	o := orm.NewOrm()
	o.Using("belajar_beego")
	
	user := models.Users{Id: id_user} // This is STRUCT Users
	err := o.Read(&user) // ORM read
	if err == orm.ErrNoRows {
    	flash.Error("No result found !") // Initialization message error (for showing to interface user)
	    flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/login", 302) // redirect to function this function if err.
	} else if err == orm.ErrMissPK {
	    flash.Error("No primary key found !") // Initialization message error (for showing to interface user)
	    flash.Store(&this.Controller) // Set Position when the function is in access
	    this.Redirect("/login", 302) // redirect to function this function if err.
	} else {

		if user.Active == 0 {

			createOn 			:= user.Created_on
			MilSecToDate 		:= time.Unix(0, createOn * int64 (time.Millisecond))
			plus3day 			:= MilSecToDate.AddDate(0, 0, 3)
			dateToMilsec 		:= plus3day.UnixNano() / int64(time.Millisecond)

			dateNowConvertToMilsec := time.Now().UnixNano() / int64(time.Millisecond)
			if dateToMilsec <= dateNowConvertToMilsec {
				email := user.Email

				var profile models.Profile // Slice Profile 
				o.QueryTable("profile").Filter("email", email).One(&profile) // Query Get Profile By email user

				pathAvatar := "./static/avatar/"+profile.Avatar // Get path file + name avatar
				os.Remove(pathAvatar) // Delete Avatar

				profileUser := models.Profile{Id_profile: profile.Id_profile} // interface profile By params Id_profile
				_, deleteProfile := o.Delete(&profileUser) // delete Profile from db
				if deleteProfile == nil { // Checking DeleteProfile Equal nil
					fmt.Println(deleteProfile) // Show Error to Terminal (CMD)
				}

				single_user := models.Users{Id: user.Id}  // Interface User By Id User
				_, deleteUser := o.Delete(&single_user) // Delete User

				if deleteUser == nil { // Checking dlete User equal nil
					flash.Error("Maybe your link has expired, please register again") //Show up message error by flash
					flash.Store(&this.Controller)
					this.Redirect("/register", 302) // Redirect Register again
				}

			} else {

				if user.Activation_code == code{

					ip := this.Ctx.Input.IP()
					user.Ip_address = ip
					user.Active 	= 1
					o.Update(&user, "ip_address", "active") // update previous active value 0 to 1

					email := user.Email

					var profile models.Profile
					o.QueryTable("profile").Filter("email", email).One(&profile) // Find Profile by email

					profileUpdate := models.Profile{Id_profile: profile.Id_profile} 
					profileUpdate.Status = 1

					_, err := o.Update(&profileUpdate, "status")

					if err == nil {
						flash.Notice("Your Account is Actived")
						flash.Store(&this.Controller)
						this.Redirect("/login", 302)
					}
				} else {
					flash.Error("Maybe your link has expired, please register again")
					flash.Store(&this.Controller)
					this.Redirect("/register", 302)
				}
			}
		}
	}
}