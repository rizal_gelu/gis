package controllers

import (
	// "fmt"
	// "log"
	"github.com/astaxie/beego"
	// "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"

    // "gis/models"
    // "golang.org/x/crypto/bcrypt"
)

type DashboardController struct {
	beego.Controller
}

func (c *DashboardController) Get() {
	c.Data["Title"] = "Login - Gelhosting"
	c.TplName 		= "forgot_password/forgot_password.tpl"
}
