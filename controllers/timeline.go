package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"gis/models"
	"errors"
)

type TimelineController struct {
	beego.Controller
	// profile *models.Profile
}

func (c *TimelineController) Get(){
	msg := "invalid email or password"
	session := c.StartSession()
	email := session.Get("Email").(string)
	// fmt.Println(email)
	profile := &models.Profile{Email: email}

	if err := profile.Read("Email"); err != nil {
		if err.Error() == "no row found" {
			err = errors.New(msg)
		}
		return
	} else {
		fmt.Println(profile)
	}

	c.Data["Website"] 	= "beego.me"
	c.Data["Title"] 	= "Timeline - GelHosting"
	c.Data["Profile"]	= profile
	c.TplName 			= "timeline/timeline.tpl"
}




















