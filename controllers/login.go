package controllers

import (
	"github.com/astaxie/beego"
    // "gis/models"
    // "gis/helpers"
    "gis/library"
    "fmt"
    // "github.com/astaxie/beego/session"
)

type LoginController struct {
	beego.Controller
	IsLogin bool
	UserId 	int
}

type LoginProcessController struct {
	beego.Controller
	// IsLogin bool
	UserId 	int
}

type LogoutController struct {
	beego.Controller
	UserId int
}

func (c *LoginController) Get() {
	session := c.StartSession()
	UserId 	:= session.Get("UserId")
	if UserId != nil {
		c.Redirect("/timeline", 302)
		return
	} else {
		flash := beego.ReadFromRequest(&c.Controller)
		if _, ok := flash.Data["error"]; ok {
			c.Data["Title"] = "Login - Gelhosting"
			c.TplName 		= "login/login.tpl"
		} else if _, ok = flash.Data["notice"]; ok {
			c.Data["Title"] = "Login - Gelhosting"
			c.TplName 		= "login/login.tpl"
	    } else {
			c.Data["Title"] = "Login - Gelhosting"
			c.TplName 		= "login/login.tpl"
		}
	}
}

func (this *LoginProcessController) Post() {
	flash 		:= beego.NewFlash() 
	email 		:= this.GetString("email")
	password 	:= this.GetString("password")

	sess 		:= this.StartSession()

	user, err 	:= library.Authenticate(email, password)
	if err != nil || user.Id < 1{
		flash.Error(err.Error())
		flash.Store(&this.Controller)
		this.Redirect("/login", 302)
	}
	_, error := library.CheckingToken(user.Id)

	if error != nil {
		fmt.Println(error)
	}

	flash.Success("Success Logged In")
	flash.Store(&this.Controller)

	sess.Set("Email", user.Email)


	this.Redirect("/timeline", 302)
}

func (this *LogoutController) Get() {
	sess := this.StartSession()
	sess.Delete("Email")
	this.Redirect("/login", 302)
}
