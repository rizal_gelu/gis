package routers

import (
	"gis/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{}) // Default Page
    beego.Router("/login", &controllers.LoginController{}) // Get Page Login
    beego.Router("/login_process", &controllers.LoginProcessController{}) // Post Process Login

    beego.Router("/register", &controllers.RegisterController{}) // Get Page Register
    beego.Router("/register_process", &controllers.RegisterProcessController{}) // Post Process Register
    beego.Router("/activation_account/:id/:code", &controllers.ActivationAccount{}) // 

    beego.Router("/forgot_password", &controllers.ForgotPasswordController{}) // Get Page Forgot Password
    beego.Router("/forgot_password_process", &controllers.ForgotPasswordProcessController{}) // Post Process Forgot Password

    beego.Router("/reset_password/:id/:code", &controllers.ResetPassword{})

    beego.Router("/dashboard",  &controllers.DashboardController{})
    
    beego.Router("/timeline", &controllers.TimelineController{})
    beego.Router("/logout", &controllers.LogoutController{})

    beego.SetStaticPath("/img", "static/img")
    beego.SetStaticPath("/css", "static/css")
    beego.SetStaticPath("/js", "static/js")
    beego.SetStaticPath("/avatar", "static/avatar")
}
