package main

import (
	_ "gis/routers"
	"github.com/astaxie/beego"
	 "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
    "github.com/astaxie/beego/session"
)

func init() {
	orm.RegisterDriver("mysql", orm.DRMySQL)
	connection := "root:@tcp(localhost:3306)/belajar_beego"
	orm.RegisterDataBase("default", "mysql", connection)
}

func main() {
	sessionconf := &session.ManagerConfig{
		CookieName: "begoosessionID",
		Gclifetime: 3600,
	}
	beego.GlobalSessions, _ = session.NewManager("memory", sessionconf)
	go beego.GlobalSessions.GC()
	
	beego.Run()
}

