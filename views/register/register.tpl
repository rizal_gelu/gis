<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="favicon.ico">
   <title>GelHosting - Register</title><!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/brands.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/regular.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/solid.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/fontawesome.css"><!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="css/admin/vendor/simple-line-icons/css/simple-line-icons.css">
   <link rel="stylesheet" href="css/admin/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="css/admin/css/bootstrap.css" id="bscss"><!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="css/admin/css/app.css" id="maincss">
</head>

<body>
   <div class="wrapper">
      <div class="block-center col-md-8">
         <!-- START card-->
         <div class="card card-flat">
            <div class="card-header text-center bg-dark"><a href="#"><img class="block-center" src="img/admin/img/logo.png" alt="Image"></a></div>
            <h3 class="text-center py-2">{{.flash.error}}</h3>
            <div class="card-body">
               <p class="text-center py-2">SIGNUP TO GET INSTANT ACCESS.</p>
               <form class="mb-3" id="register" method="post" action="/register_process" enctype="multipart/form-data">
                <div class="row">
               		<div class="form-group col-md-4">
               			<label class="text-muted" for="signupInputEmail1">First Name</label>
                   	<div class="input-group with-focus">
                   		<input class="form-control" type="text" placeholder="First Name" autocomplete="off" name="first_name" required>
                   	</div>
                  </div>

                  <div class="form-group col-md-4">
               			<label class="text-muted" for="signupInputEmail1">Last Name</label>
                   	<div class="input-group with-focus">
                   		<input class="form-control" type="text" placeholder="Last Name" autocomplete="off" name="last_name" required>
                   	</div>
                  </div>
                  	
                  <div class="form-group col-md-4">
                  	<label class="text-muted" for="signupInputEmail1">Email address</label>
                 		<div class="input-group with-focus">
                 			<input class="form-control border-right-0" id="signupInputEmail1" type="email" name="email" placeholder="Enter email" autocomplete="off" required>
                    	<div class="input-group-append">
                    		<span class="input-group-text text-muted bg-transparent border-left-0">
                  				<em class="fa fa-envelope"></em>
                  			</span>
                  		</div>
               		  </div>
                	</div>

                  <div class="form-group col-md-6">
                    <label class="text-muted" for="signupInputPassword1">Password</label>
                    <div class="input-group with-focus">
                      <input class="form-control border-right-0" id="signupInputPassword1" type="password" name="password" placeholder="Password" autocomplete="off" required>
                        <div class="input-group-append">
                          <span class="input-group-text text-muted bg-transparent border-left-0">
                            <em class="fa fa-lock"></em>
                          </span>
                        </div>
                     </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label class="text-muted" for="signupInputRePassword1">Confirm Password</label>
                    <div class="input-group with-focus">
                      <input class="form-control border-right-0" id="signupInputRePassword1" type="password" name="password" placeholder="Retype Password" autocomplete="off" required data-parsley-equalto="#signupInputPassword1">
                        <div class="input-group-append">
                          <span class="input-group-text text-muted bg-transparent border-left-0">
                            <em class="fa fa-lock"></em>
                          </span>
                        </div>
                     </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label class="text-muted" for="signupInputEmail1">Security Question</label>
                    <div class="input-group with-focus">
                      <select class="form-control" name="id_security_question">
                        <option>Security Question</option>
                          {{range $key, $val := .Question}}
                            <option value="{{$val.Id_security_question}}">{{$val.Question}}</option>
                          {{end}}
                      </select>
                    </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label class="text-muted" for="signupInputEmail1">Answer Question</label>
                    <div class="input-group with-focus">
                      <input class="form-control" type="text" placeholder="Answer" autocomplete="off" name="answer" required>
                    </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label class="text-muted" for="signupInputEmail1">Date Of Birth</label>

                    <div class="input-group date" id="datetimepicker1">
                      <input class="form-control" type="text" name="date_of_birth">
                      <span class="input-group-append input-group-addon"><span class="input-group-text fas fa-calendar-alt"></span></span>
                    </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label class="text-muted" for="signupInputEmail1">Avatar</label>
                    <input id="avatar" class="form-control filestyle" type="file" name="avatar" data-classbutton="btn btn-secondary" data-classinput="form-control inline" data-icon="&lt;span class='fa fa-upload mr-2'&gt;&lt;/span&gt;">
                  </div>

                  <div class="form-group col-md-6">
                    <div class="checkbox c-checkbox mt-0">
                    	<label>
                  			<input type="checkbox" value="" required name="agreed">
    		          			<span class="fa fa-check"></span> I agree with the
    		          			<a class="ml-1" href="#">terms</a>
    		          		</label>
                    </div>
                  </div>

                	<button class="btn btn-block btn-primary mt-3" type="submit">Create account</button>
                </div>  
               </form>
               
               <p class="pt-3 text-center">Have an account?</p>
               <a class="btn btn-block btn-secondary" href="/login">Sign In</a>
            </div>
        </div><!-- END card-->
	    <div class="p-3 text-center">
	    	<span class="mr-2">&copy;</span>
	    	<span>2019</span>
	    	<span class="mr-2">-</span>
	    	<span>GelHosting</span><br>
	    	<span>Gelu[R]</span>
	   	</div>
    </div>
</div>
	<!-- =============== VENDOR SCRIPTS ===============-->
   	<!-- MODERNIZR-->
   	<script src="css/admin/vendor/modernizr/modernizr.custom.js"></script>
   	<!-- STORAGE API-->
   	<script src="css/admin/vendor/js-storage/js.storage.js"></script><!-- i18next-->
   	<script src="css/admin/vendor/i18next/i18next.js"></script>
   	<script src="css/admin/vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- JQUERY-->
   	<script src="css/admin/vendor/jquery/dist/jquery.js"></script><!-- BOOTSTRAP-->
   	<script src="css/admin/vendor/popper.js/dist/umd/popper.js"></script>
   	<script src="css/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
    <script src="css/admin/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <script src="css/admin/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js"></script><!-- TAGS INPUT-->
    <script src="css/admin/admin/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script><!-- CHOSEN-->
    <script src="css/admin/vendor/chosen-js/chosen.jquery.js"></script><!-- SLIDER CTRL-->
    <script src="css/admin/vendor/bootstrap-slider/dist/bootstrap-slider.js"></script><!-- INPUT MASK-->
    <script src="css/admin/vendor/inputmask/dist/jquery.inputmask.bundle.js"></script><!-- WYSIWYG-->
    <script src="css/admin/vendor/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script><!-- MOMENT JS-->
    <script src="css/admin/vendor/moment/min/moment-with-locales.js"></script><!-- DATETIMEPICKER-->
    <script src="css/admin/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script><!-- COLORPICKER-->
    <script src="css/admin/vendor/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script><!-- SELECT2-->
    <script src="css/admin/vendor/select2/dist/js/select2.full.js"></script><!-- =============== APP SCRIPTS ===============-->
    <!-- PARSLEY-->
   	<script src="css/admin/vendor/parsleyjs/dist/parsley.js"></script>
   	<!-- =============== APP SCRIPTS ===============-->
   	<script src="css/admin/js/app.js"></script>
</body>

</html>