<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="favicon.ico">
   <title>{{.Title}}</title><!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/brands.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/regular.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/solid.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/fontawesome.css"><!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="css/admin/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="css/admin/css/bootstrap.css" id="bscss"><!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="css/admin/css/app.css" id="maincss">
</head>