<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="favicon.ico">
   <title>{{.Title}}</title><!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/brands.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/regular.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/solid.css">
   <link rel="stylesheet" href="css/admin/vendor/@fortawesome/fontawesome-free/css/fontawesome.css"><!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="css/admin/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="css/admin/css/bootstrap.css" id="bscss"><!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="css/admin/css/app.css" id="maincss">
</head>

<body>
   <div class="wrapper">
      <div class="block-center mt-4 wd-xl">
         <!-- START card-->
         <div class="card card-flat">
            <div class="card-header text-center bg-dark"><a href="#"><img class="block-center rounded" src="img/admin/img/logo.png" alt="Image"></a></div>
            <div class="card-body">
               <p class="text-center py-2">PASSWORD RESET</p>
               <form method="post" action="/forgot_password_process">
                  <p class="text-center">Fill with your mail to receive instructions on how to reset your password.</p>
                  <div class="form-group"><label class="text-muted" for="resetInputEmail1">Email address</label>
                     <div class="input-group with-focus"><input class="form-control border-right-0" id="forgot_password" type="email" placeholder="Enter email" autocomplete="off" name="email" required>
                        <div class="input-group-append"><span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-envelope"></em></span></div>
                     </div>
                  </div><button class="btn btn-danger btn-block" type="submit">Reset</button>
               </form>
            </div>
         </div><!-- END card-->
         <div class="p-3 text-center"><span class="mr-2">&copy;</span><span> 2019 </span><span class="mr-2">-</span><span>GelHosting</span><br><span>Gelu[R]</span></div>
      </div>
   </div><!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="css/admin/vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
   <script src="css/admin/vendor/js-storage/js.storage.js"></script><!-- i18next-->
   <script src="css/admin/vendor/i18next/i18next.js"></script>
   <script src="css/admin/vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- JQUERY-->
   <script src="css/admin/vendor/jquery/dist/jquery.js"></script><!-- BOOTSTRAP-->
   <script src="css/admin/vendor/popper.js/dist/umd/popper.js"></script>
   <script src="css/admin/vendor/bootstrap/dist/js/bootstrap.js"></script><!-- PARSLEY-->
   <script src="css/admin/vendor/parsleyjs/dist/parsley.js"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="css/admin/js/app.js"></script>
</body>

</html>