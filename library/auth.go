package library

import (
	// "fmt"
	"errors"
	"time"
	"gis/models"
	"gis/helpers"
	// "github.com/astaxie/beego/httplib"
)

type Sess struct {
	Userinfo *models.Users
	IsLogin bool
}

func Authenticate(email string, password string) (user *models.Users, err error) {
	msg := "invalid email or password"
	user = &models.Users{Email: email}

	if err := user.Read("Email"); err != nil {
		if err.Error() == "no row found" {
			err = errors.New(msg)
		}
		return user, err
	} else if user.Id < 1 {
		return user, errors.New(msg)
	} else if !helpers.CheckPasswordHash(password, user.Password) {
		return user, errors.New(msg)
	} else {
		message := "This Account not Active. Please Actived first"
		if user.Active == 0 {
			return user, errors.New(message)
		} else {
			user.Last_login = time.Now().UnixNano() / int64(time.Millisecond)
			user.Update("Last_login")
			return user, nil
		}
	}
}

// func (m *Sess) GetLogin (isLogin bool) {
// 	u := &models.Users{Id: m.GetSession("userinfo").(int)}
// 	u.Read()
// 	return u
// } 

// func (m *Sess) SetLogin(user *models.Users) {
// 	m.SetSession("userinfo", user.Id)
// 	return m
// }

func CheckingToken(id_user int) (jwt *models.Security_keys, err error) {
	jwt = &models.Security_keys{Id_user: id_user}

	// uid := GetSession(id_user)
	if err := jwt.Read("Id_user"); err != nil {
		jwt.Token 			= helpers.RandomString(40)
		jwt.Id_user 		= id_user
		jwt.Date_created 	= time.Now().UnixNano() / int64(time.Millisecond)
		jwt.Insert()

		return jwt, err
	} else {
		dateCreatedToMilSec 	:= time.Unix(0, jwt.Date_created * int64 (time.Millisecond))
		plusOneday 				:= dateCreatedToMilSec.AddDate(0, 0, 1)
		dateNowMilSec 			:= time.Now().UnixNano() / int64(time.Millisecond)

		dateToMilsec 			:= plusOneday.UnixNano() / int64(time.Millisecond)

		if dateToMilsec <= dateNowMilSec {
			jwt.Token 			= helpers.RandomString(40)
			jwt.Date_created	= time.Now().UnixNano() / int64(time.Millisecond)
			jwt.Update("Token", "Date_created")
		} 
		return jwt, nil
	}
}

func SignUpUser(u *models.Users) (int, error) {
	var (
		err error
		msg string
	)

	if models.User_check().Filter("email", u.Email).Exist() {
		msg = "was already registered input email address"
		return 0, errors.New(msg)
	}

	helpers.HashPassword(u.Password)

	err = u.Insert()
	if err != nil {
		return 0, err
	}

	return u.Id, err
}